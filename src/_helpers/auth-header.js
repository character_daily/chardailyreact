export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.result.accessToken) {
        return { 'Authorization': 'Bearer ' + user.result.accessToken };
    } else {
        return {};
    }
}