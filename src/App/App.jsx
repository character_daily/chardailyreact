import React, { Component } from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { Lessons } from '../Lessons';
import { CharacterTools } from '../CharacterTools';
import { Students } from '../Students';
import { Classrooms } from '../Classrooms';
import { LoginPage } from '../LoginPage';
var CanvasJSReact = require('../scripts/canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
class App extends React.Component {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;

        
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });

    }
    
    render() {
        const { alert } = this.props;
        
        return (
            <div>
                {
                    alert.message &&
                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                }
                <Router history={history}>
                    <div className="fortify">
                        {routes.map((route, i) => (
                            <RouteWithSubRoutes key={i} {...route} />
                        ))}
                        <Route path="/login" component={LoginPage} />
                    </div>
                </Router>                    
            </div>
        );
    }
}

const routes = [    
    {
      path: "/",
      component: HomePage,
      routes: null
    },
    {
      path: "/Lessons",
      component: Lessons,
      routes: null
    },
    {
        path: "/CharacterTools",
        component: CharacterTools,
        routes: null
    },
    {
        path: "/Classrooms",
        component: Classrooms,
        routes: null
    },
    {
        path: "/Students",
        component: Students,
        routes: null
    }
  ];

// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
function RouteWithSubRoutes(route) {
    return (
      <PrivateRoute
        exact
        path={route.path}
        component={route.component}
        render={props => (
          // pass the sub-routes down to keep nesting
          <route.component {...props} routes={route.routes} />
        )}
      />
    );
  }

// map alerts to app
function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 