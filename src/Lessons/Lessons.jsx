import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Header } from '../Header';
import { Navigation } from '../Navigation';
import { LessonsPage } from '../LessonsPage';
class Lessons extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    render() {
        const { user, users } = this.props;
        return (
            <div>
                <Header></Header>    
                <div className="container-fluid">
                    <div className="row">
                        <Navigation></Navigation>
                        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                            <LessonsPage></LessonsPage>
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedLessons = connect(mapStateToProps)(Lessons);
export { connectedLessons as Lessons };