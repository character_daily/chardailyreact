import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class LoginModal extends React.Component {
    render() {   
        // Render nothing if the "show" prop is false
        if(!this.props.show) {
            return null;
        }
        // The gray background
        const backdropStyle = {
            position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 50
        };
    
        // The modal "window"
        const modalStyle = {
            backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 500,      
            height:250,      
            margin: '0 auto',
            padding: 0,
            display: 'block',
            marginTop: 100
        };
           
        const dialogStyle = {
            marginTop: 0
        }

        const footerStyle = {
            height:50
        }

        return (
            <div className="backdrop" style={ backdropStyle }>
                <form name="form" onSubmit={ this.props.onSubmit }>
                    <div className="modal" style={modalStyle }>
                        <div className="modal-dialog" role="document" style={dialogStyle}>
                            <div className="modal-content"> 
                                <div className="modal-header login-header">
                                    <h5 className="modal-title clear-title">Login</h5>                
                                    <button type="button" className="close" aria-label="Close" onClick={ this.props.onClose }> 
                                        <span aria-hidden="true">&times;</span>
                                    </button>                    
                                </div>
                                <div className="modal-body login-block">
                                    { this.props.children }
                                </div>
                                <div className="modal-footer" style={footerStyle}>
                                    <span><sup>(c)</sup> 2018 - The Canton Group</span>                        
                                    <button className="btn btn-dark btn-sm">Login</button>      
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
  }
  
  LoginModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    show: PropTypes.bool,
    username: PropTypes.any,
    password: PropTypes.any,
    children: PropTypes.node
  };
