import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
export class Navigation extends React.Component {
    render() {
        return (
            <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                <div className="sidebar-sticky">                    
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <a className="nav-link active" href="/">
                            <span data-feather="home"></span>
                            Home <span className="sr-only">(current)</span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="Lessons">
                            <span data-feather="file"></span>
                            Lessons
                            </a>
                        </li>                           
                        <li className="nav-item">
                            <a className="nav-link" href="CharacterTools">
                            <span data-feather="users"></span>
                            Character Tools
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="Classrooms">
                            <span data-feather="layers"></span>
                            Classrooms
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="Students">
                            <span data-feather="layers"></span>
                            Students
                            </a>
                        </li>
                    </ul>
                    <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Saved reports</span>
                        <a className="d-flex align-items-center text-muted" href="#">
                            <span data-feather="plus-circle"></span>
                        </a>
                    </h6>
                    <ul className="nav flex-column mb-2">
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Current month
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Last quarter
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Social engagement
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                            <span data-feather="file-text"></span>
                            Human Resources
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>    
        );
    }
}

