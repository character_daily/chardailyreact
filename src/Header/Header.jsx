import React from 'react';
import { Link } from 'react-router-dom';
export class Header extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="https://cantongroup.com">Canton Group</a>
                <input className="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"></input>
                <ul className="navbar-nav px-3">
                    <li className="nav-item text-nowrap">                           
                        <Link to="/login">Logout</Link>
                    </li>
                </ul>
            </nav>      
        );
    }
}