import { userConstants } from '../_constants';

export function userContent(state = {}, action) {
  switch (action.type) {
    case userConstants.GETCONTENT_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETCONTENT_SUCCESS:
      return {
        items: action.items
      };
    case userConstants.GETCONTENT_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}