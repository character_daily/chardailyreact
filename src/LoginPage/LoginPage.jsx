import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { LoginModal } from '../_components/login';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.props.dispatch(userActions.logout());
        this.state = {
            username: '',
            password: '',
            submitted: false,
            isOpen: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    toggleModal (e) {
        e.preventDefault();
        this.setState({
          isOpen: !this.state.isOpen
        });
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted, isOpen } = this.state;
        return (
            <div className="text-center">
            <canvas id="canvas"></canvas>
            {/* <canvas id="crosscanvas"></canvas>   */}            
                <form className="form-signin" onSubmit={this.handleSubmit}>
                    <img className="mb-4" src="../src/images/logo-color.png" alt="" width="auto" height="72" />
                    <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                    <label className="sr-only">User Name</label>
                    <input type="text" className="form-control" id="username" name="username" placeholder="Username" value={ this.props.username } onChange={this.handleChange} aria-describedby="basic-addon3" />
                    <label className="sr-only">Password</label>
                    <input type="password" className="form-control" id="password" name="password" placeholder="Password" value={ this.props.password } onChange={this.handleChange} /> 
                    <div className="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember-me" /> Remember me
                        </label>
                    </div>
                    <button className="btn btn-lg btn-light btn-block" type="submit">Sign in</button>
                    <p className="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
                </form>            
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 