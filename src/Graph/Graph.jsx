import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { Customer } from '../Customer';
var React1 = require('react');
var Component = React1.Component;
var CanvasJSReact = require('../scripts/canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
class CGGraph extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    render() {
        const { user, users } = this.props;
        //const Samples = {};
        var chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        const Months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        const COLORS = [
            '#4dc9f6',
            '#f67019',
            '#f53794',
            '#537bc4',
            '#acc236',
            '#166a8f',
            '#00a950',
            '#58595b',
            '#8549ba'
        ];
        const config = {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'Request for Information',
                    borderColor: chartColors.red,
                    backgroundColor: chartColors.red,
                    data: [
                        51,
                        68,
                        124,
                        155,
                        162,
                        12,
                        44
                    ],
                }, {
                    label: 'Request for Proposals',
                    borderColor: chartColors.blue,
                    backgroundColor: chartColors.blue,
                    data: [
                        51,
                        68,
                        124,
                        155,
                        162,
                        12,
                        44
                    ],
                }, {
                    label: 'Request for Quotations',
                    borderColor: chartColors.green,
                    backgroundColor: chartColors.green,
                    data: [
                        51,
                        68,
                        124,
                        155,
                        162,
                        12,
                        44
                    ],
                }, {
                    label: 'Submittals',
                    borderColor: chartColors.yellow,
                    backgroundColor: chartColors.yellow,
                    data: [
                        51,
                        68,
                        124,
                        155,
                        162,
                        12,
                        44
                    ],
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Integration Graphs'
                },
                tooltips: {
                    mode: 'index',
                },
                hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };
        const options = {
			animationEnabled: true,
			exportEnabled: true,
			theme: "light2", //"light1", "dark1", "dark2"
			title:{
				text: "Simple Column Chart with Index Labels"
			},
			data: [{
				type: 'line',
				//indexLabel: "{y}", //Shows y value on all Data Points
				indexLabelFontColor: "#5A5757",
				indexLabelPlacement: "outside",
				dataPoints: [
					{ x: 10, y: 71 },
					{ x: 20, y: 55 },
					{ x: 30, y: 50 },
					{ x: 40, y: 65 },
					{ x: 50, y: 71 },
					{ x: 60, y: 68 },
					{ x: 70, y: 38 },
					{ x: 80, y: 92, indexLabel: "Highest" },
					{ x: 90, y: 54 },
					{ x: 100, y: 60 },
					{ x: 110, y: 21 },
					{ x: 120, y: 49 },
					{ x: 130, y: 36 }
				]
			}]
		}
        return (
            <div>      
                <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                    <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="https://cantongroup.com">Canton Group</a>
                    <input className="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"></input>
                    <ul className="navbar-nav px-3">
                        <li className="nav-item text-nowrap">                           
                            <Link to="/login">Logout</Link>
                        </li>
                    </ul>
                </nav>      
                <div className="container-fluid">
                    <div className="row">
                        <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                        <div className="sidebar-sticky">
                            <ul className="nav flex-column">
                            <li className="nav-item">
                                <a className="nav-link active" href="/">
                                <span data-feather="home"></span>
                                Dashboard <span className="sr-only">(current)</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="Submittals">
                                <span data-feather="file"></span>
                                Submittals
                                </a>
                            </li>                           
                            <li className="nav-item">
                                <a className="nav-link" href="Customers">
                                <span data-feather="users"></span>
                                Customers
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="Prospects">
                                <span data-feather="layers"></span>
                                Prospects
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="Reports">
                                <span data-feather="bar-chart-2"></span>
                                Reports
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="Integrations">
                                <span data-feather="layers"></span>
                                Integrations
                                </a>
                            </li>
                            </ul>

                            <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Saved reports</span>
                            <a className="d-flex align-items-center text-muted" href="#">
                                <span data-feather="plus-circle"></span>
                            </a>
                            </h6>
                            <ul className="nav flex-column mb-2">
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                <span data-feather="file-text"></span>
                                Current month
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                <span data-feather="file-text"></span>
                                Last quarter
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                <span data-feather="file-text"></span>
                                Social engagement
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">
                                <span data-feather="file-text"></span>
                                Human Resources
                                </a>
                            </li>
                            </ul>
                        </div>
                    </nav>
                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h1 className="h2">Dashboard</h1>
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <div className="btn-group mr-2">
                                <button className="btn btn-sm btn-outline-secondary">Share</button>
                                <button className="btn btn-sm btn-outline-secondary">Export</button>
                            </div>
                            <button className="btn btn-sm btn-outline-secondary dropdown-toggle">
                                <span data-feather="calendar"></span>
                                This week
                            </button>
                        </div>
                    </div>
                    <div>
			            <CanvasJSChart options = {options} 
				             onRef={ref => this.chart = ref} 
			            />
			            {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		            </div>                    
                    <h2>Section title</h2>
                        <div className="table-responsive">
                            <table className="table table-striped table-sm">
                                <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>Header</th>
                                    <th>Header</th>
                                    <th>Header</th>
                                    <th>Header</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td>1,001</td>
                                    <td>Lorem</td>
                                    <td>ipsum</td>
                                    <td>dolor</td>
                                    <td>sit</td>
                                    </tr>
                                    <tr>
                                    <td>1,002</td>
                                    <td>amet</td>
                                    <td>consectetur</td>
                                    <td>adipiscing</td>
                                    <td>elit</td>
                                    </tr>
                                    <tr>
                                    <td>1,003</td>
                                    <td>Integer</td>
                                    <td>nec</td>
                                    <td>odio</td>
                                    <td>Praesent</td>
                                    </tr>
                                    <tr>
                                    <td>1,003</td>
                                    <td>libero</td>
                                    <td>Sed</td>
                                    <td>cursus</td>
                                    <td>ante</td>
                                    </tr>
                                    <tr>
                                    <td>1,004</td>
                                    <td>dapibus</td>
                                    <td>diam</td>
                                    <td>Sed</td>
                                    <td>nisi</td>
                                    </tr>
                                    <tr>
                                    <td>1,005</td>
                                    <td>Nulla</td>
                                    <td>quis</td>
                                    <td>sem</td>
                                    <td>at</td>
                                    </tr>
                                    <tr>
                                    <td>1,006</td>
                                    <td>nibh</td>
                                    <td>elementum</td>
                                    <td>imperdiet</td>
                                    <td>Duis</td>
                                    </tr>
                                    <tr>
                                    <td>1,007</td>
                                    <td>sagittis</td>
                                    <td>ipsum</td>
                                    <td>Praesent</td>
                                    <td>mauris</td>
                                    </tr>
                                    <tr>
                                    <td>1,008</td>
                                    <td>Fusce</td>
                                    <td>nec</td>
                                    <td>tellus</td>
                                    <td>sed</td>
                                    </tr>
                                    <tr>
                                    <td>1,009</td>
                                    <td>augue</td>
                                    <td>semper</td>
                                    <td>porta</td>
                                    <td>Mauris</td>
                                    </tr>
                                    <tr>
                                    <td>1,010</td>
                                    <td>massa</td>
                                    <td>Vestibulum</td>
                                    <td>lacinia</td>
                                    <td>arcu</td>
                                    </tr>
                                    <tr>
                                    <td>1,011</td>
                                    <td>eget</td>
                                    <td>nulla</td>
                                    <td>Class</td>
                                    <td>aptent</td>
                                    </tr>                                    
                                </tbody>
                            </table>
                        </div>
                    </main>
                </div>
            </div>
        </div>
            // <div className="col-md-6 col-md-offset-3">
            //     <h1>Hi {user.firstName}!</h1>
            //     <p>You're logged in with React & JWT!!</p>
            //     <h3>Users from secure api end point:</h3>
            //     {users.loading && <em>Loading users...</em>}
            //     {users.error && <span className="text-danger">ERROR: {users.error}</span>}
            //     {users.items &&
            //         <ul>
            //             {users.items.map((user, index) =>
            //                 <li key={user.id}>
            //                     {user.firstName + ' ' + user.lastName}
            //                 </li>
            //             )}
            //         </ul>
            //     }
            //     <p>
            //         <Link to="/login">Logout</Link>
            //     </p>
            // </div>           
        );
    }

    
      
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedCGGraph = connect(mapStateToProps)(CGGraph);
export { connectedCGGraph as CGGraph };